package kit.upgrades.plug.enums;

public enum Slot {
SLOT_1, SLOT_2, SLOT_3;

public static Integer convertSlot(Slot slot){
	switch(slot){
	case SLOT_1:
		return 0;
	case SLOT_2:
		return 1;
	case SLOT_3:
		return 2;
	default:
		return null;
	
	}
}
}
