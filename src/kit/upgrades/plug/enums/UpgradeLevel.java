package kit.upgrades.plug.enums;

import kit.upgrades.plug.files.KitDataManager;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public enum UpgradeLevel {
Level1, Level2, Level3, Level4, Level5;
public static Integer convertLevel(UpgradeLevel level){
	switch(level){
	case Level1:
		return 1;
	case Level2:
		return 2;
	case Level3:
		return 3;
	case Level4:
		return 4;
	case Level5:
		return 5;
	default:
		return null;
	
	}
}
public static UpgradeLevel convertLevel(Integer level){
	switch(level){
	case 1:
		return UpgradeLevel.Level1;
	case 2:
		return UpgradeLevel.Level2;
	case 3:
		return UpgradeLevel.Level3;
	case 4:
		return UpgradeLevel.Level4;
	case 5:
		return UpgradeLevel.Level5;
	default:
		return null;
	
	}
}
public static ItemStack getSword(Player player){
	UpgradeLevel level = KitDataManager.playerLevelMap.get(player.getUniqueId()).get(UpgradeType.SWORD);
	switch(level){
	case Level1:
		return new ItemStack(Material.WOOD_SWORD);
	case Level2:
		return new ItemStack(Material.STONE_SWORD);
	case Level3:
		return new ItemStack(Material.IRON_SWORD);
	case Level4:
		return new ItemStack(Material.GOLD_SWORD);
	case Level5:
		return new ItemStack(Material.DIAMOND_SWORD);
	default:
		return null;
	
	}
}
}

