package kit.upgrades.plug.enums;

import kit.upgrades.plug.files.KitDataManager;

import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;



public enum Relic {
HEALTH, STAMINA, STRENGTH;

public static Relic getRelicFromItemStack(ItemStack item){
	ItemMeta im = item.getItemMeta();
	String name = im.getDisplayName().split("//n").length > 0 ? im.getDisplayName().split("//n")[0] : null;
	
	if (name.equals("null")){
		return null;
	}
	System.out.println(name.toUpperCase());
	return null;
	
}
@SuppressWarnings("deprecation")
public static ItemStack getRelicItem(Relic relic){
		String [] data = KitDataManager.configRelicMap.get(relic).get(RelicConfigType.ITEM).split(",");
		ItemStack item = new ItemStack(Integer.valueOf(data[0]), 1, Short.valueOf(data[1]));
		ItemMeta im = item.getItemMeta();
		im.setDisplayName(data[2]);
		item.setItemMeta(im);
		return item;
}
public static Double getHealthRegenAmount(){
	return Double.valueOf(KitDataManager.configRelicMap.get(Relic.HEALTH).get(RelicConfigType.REGEN));
}
public static Integer getManaRegenAmount(){
	return Integer.valueOf(KitDataManager.configRelicMap.get(Relic.STAMINA).get(RelicConfigType.REGEN));
}
public static Double getStrengthDamageAmount(){
	return Double.valueOf(KitDataManager.configRelicMap.get(Relic.STRENGTH).get(RelicConfigType.DAMAGE));
}

}
