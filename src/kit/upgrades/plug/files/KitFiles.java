package kit.upgrades.plug.files;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.net.URL;

import org.bukkit.Bukkit;

public class KitFiles {
	
	public static File getPluginDataFolder(){
		return Bukkit.getPluginManager().getPlugin("KitUpgrades").getDataFolder();
		}
	
	public static File getKitDataFolder(){
		return new File(Bukkit.getPluginManager().getPlugin("KitUpgrades").getDataFolder(), "/data");
		}
	
	public static File getPlayerPointsDataFile(){
		return new File(getKitDataFolder(), "/ppd.dat");
		}
	
	public static File getPlayerLevelDataFile(){
		return new File(getKitDataFolder(), "/pld.dat");
		}
	
	public static File getPlayerDamageDataFile(){
		return new File(getKitDataFolder(), "/pdd.dat");
		}
	
	public static File getConfigFile(){
		return new File(getPluginDataFolder(), "/config.yml");
		}
	
	public static void exportResource(URL resourceURL, File dest){
        try {
        	dest.createNewFile();
            BufferedReader in = new BufferedReader(new InputStreamReader(resourceURL.openStream()));
            BufferedWriter out = new BufferedWriter(new FileWriter(dest));
            String line = "";
            while((line = in.readLine()) != null){
            	out.write(line);
            	out.newLine();
            	out.flush();
            }
            in.close();
            out.close();

        }catch(Exception e){
        	e.printStackTrace();
        }
	}
}
