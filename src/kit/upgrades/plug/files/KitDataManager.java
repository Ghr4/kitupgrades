package kit.upgrades.plug.files;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import kit.upgrades.plug.enums.GUIConfigType;
import kit.upgrades.plug.enums.GeneralConfigType;
import kit.upgrades.plug.enums.Relic;
import kit.upgrades.plug.enums.RelicConfigType;
import kit.upgrades.plug.enums.Slot;
import kit.upgrades.plug.enums.UpgradeLevel;
import kit.upgrades.plug.enums.UpgradeType;
import kit.upgrades.plug.enums.WorldType;
import kit.upgrades.plug.main.KitUpgrades;

import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;

public class KitDataManager implements Serializable{
	static File ppd = KitFiles.getPlayerPointsDataFile();
	static File pdd = KitFiles.getPlayerDamageDataFile();
	static File pld = KitFiles.getPlayerLevelDataFile();
	
	/**
	 * Use this ID to translate any loaded or saved data from within KitUpgrades
	 */
	private static final long serialVersionUID = 1718088346412007037L;
	
	
	
	/*
	 * Variables
	 */
	private static String[] upgradeType = {"Mana", "Health", "Sword", "Bow"};
	private static String[] guiConfigType = {"Inventory Trigger", "Slots", "Fill Block", "Upgrade Items"};
	private static String[] generalConfigType = {"Cartel World"};
	private static String[] worldTypeConfigType = {"Cartel", "Default"};
	private static String[] slotConfigType = {"Slot 1", "Slot 2", "Slot 3"};
	private static String[] relic = {"Health", "Stamina", "Strength"};
	private static String[] relicConfigType = {"Item", "Damage", "Regen"};
	
	
	
	public static HashMap<UUID, Integer> playerPointDataMap = new HashMap<UUID, Integer>();
	public static HashMap<UUID, HashMap<UUID, Double>> playerDamageMap = new HashMap<UUID, HashMap<UUID, Double>>();
	public static HashMap<UUID, HashMap<UpgradeType, UpgradeLevel>> playerLevelMap= new HashMap<UUID, HashMap<UpgradeType, UpgradeLevel>>();
	public static HashMap<UUID, Integer> playerCurrentMana = new HashMap<UUID, Integer>();
	public static HashMap<Location, List<ItemStack>> playerDeathItems = new HashMap<Location, List<ItemStack>>();
	public static HashMap<UUID, List<ItemStack>> playerDeathKeptItems = new HashMap<UUID, List<ItemStack>>();
	
	public static HashMap<UpgradeType, HashMap<UpgradeLevel, String>> configLevelsMap = new HashMap<UpgradeType, HashMap<UpgradeLevel, String>>();
	public static HashMap<GUIConfigType, String> configGUIMap = new HashMap<GUIConfigType, String>();
	public static HashMap<GeneralConfigType, String> configGeneralMap = new HashMap<GeneralConfigType, String>();
	public static HashMap<WorldType, HashMap<Slot, String>> configSlotItemsMap = new HashMap<WorldType, HashMap<Slot, String>>();
	public static HashMap<Relic, HashMap<RelicConfigType, String>> configRelicMap = new HashMap<Relic, HashMap<RelicConfigType, String>>();
	public static HashMap<UpgradeType, String> configItemsGUIMap = new HashMap<UpgradeType, String>();
	
	
	
	public static Integer defaultPoints;
	
	
	
	
	
	/*
	 * Load Method
	 */
	@SuppressWarnings("unchecked")
	public static void loadAllData(boolean checkFiles){
		
		if (checkFiles){
			checkFiles();
		}
		
		/*
		 * Load our Player Points Map into memory.
		 */
		try{
			FileInputStream fis = new FileInputStream(KitFiles.getPlayerPointsDataFile());
			ObjectInputStream ois = new ObjectInputStream(fis);
			playerPointDataMap = (HashMap<UUID, Integer>) ois.readObject();
			ois.close();
		}catch(EOFException e){
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		try{
			FileInputStream fis = new FileInputStream(KitFiles.getPlayerLevelDataFile());
			ObjectInputStream ois = new ObjectInputStream(fis);
			playerLevelMap =  (HashMap<UUID, HashMap<UpgradeType, UpgradeLevel>>) ois.readObject();
			ois.close();
		}catch(EOFException e){
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		try{
			FileInputStream fis = new FileInputStream(KitFiles.getPlayerDamageDataFile());
			ObjectInputStream ois = new ObjectInputStream(fis);
			playerDamageMap =  (HashMap<UUID, HashMap<UUID, Double>>) ois.readObject();
			ois.close();
		}catch(EOFException e){
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		File customYmlSkills = KitFiles.getConfigFile();
		FileConfiguration config = YamlConfiguration.loadConfiguration(customYmlSkills);
		/*
		 * Load the [General] configuration values into memory.
		 */
		
		defaultPoints = config.getInt("Default Points");
		
		
		/*
		 * Load the [LEVELS] configuration values into memory.
		 */
		
		for (String upgradeType : upgradeType){
			HashMap<UpgradeLevel, String> data = new HashMap<UpgradeLevel, String>();
			for (int i = 1; i<6;i++){
				data.put(UpgradeLevel.valueOf("Level"+String.valueOf(i)), config.getString(upgradeType + "." + "Level " + i));
			}
			configLevelsMap.put(UpgradeType.valueOf(upgradeType.toUpperCase()), data);
		}
		
		/*
		 * Load the [GUI] configuration values into memory.
		 */
		
		for (String guiType : guiConfigType){
			if(guiType.equals("Upgrade Items")){
				for(String upType : upgradeType){
					configItemsGUIMap.put(UpgradeType.valueOf(upType.toUpperCase()), config.getString(guiType + "." + upType));
				}
			}else{
				configGUIMap.put(GUIConfigType.valueOf(guiType.replace(" ", "")), config.getString(guiType));
			}
		}
		
		/*
		 * Load the [Relic] configuration values into memory.
		 */
		for(String r : relic){
			HashMap<RelicConfigType, String> map = new HashMap<RelicConfigType, String>();
			for(String rct : relicConfigType){
				String data = config.getString("Relics." + r + " Relic." + rct);
				map.put(RelicConfigType.valueOf(rct.toUpperCase()), data);
			}
			configRelicMap.put(Relic.valueOf(r.toUpperCase()), map);
		}
		
		
		/*
		 * Load the [General] configuration values into memory.
		 */
		for(String genType : generalConfigType){
			if(genType.equals("Cartel World")){
				configGeneralMap.put(GeneralConfigType.valueOf(genType.toUpperCase().replace(" ", "_")), config.getString(genType));
			}
		}
		for(String type : worldTypeConfigType){
			HashMap<Slot, String> map = new HashMap<Slot, String>();
			for(String slot : slotConfigType){
				Slot s = Slot.valueOf(slot.toUpperCase().replace(" ", "_"));
				String path = type.equals("Cartel") ? config.getString("Default Items.Cartel World." + slot) : config.getString("Default Items." + type + "." + slot);
				map.put(s, path);
			}
			configSlotItemsMap.put(WorldType.valueOf(type.toUpperCase()), map);
		}
		
	}
	
	
	
	/*
	 * Save Method
	 */
	public static void saveAllData(){
		System.out.println("[KitUpgrades] Saving Data.");
		ObjectOutputStream oos;
		try {
		    oos = new ObjectOutputStream(new FileOutputStream(ppd, false));
	        oos.writeObject(KitDataManager.playerPointDataMap);
	        oos.flush();
	        oos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		try {
		    oos = new ObjectOutputStream(new FileOutputStream(pdd, false));
	        oos.writeObject(KitDataManager.playerDamageMap);
	        oos.flush();
	        oos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		try {
		    oos = new ObjectOutputStream(new FileOutputStream(pld, false));
	        oos.writeObject(KitDataManager.playerLevelMap);
	        oos.flush();
	        oos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
	}
	
	
	
	public static void checkFiles(){
		File kitDataFolder = KitFiles.getKitDataFolder();
		File pluginDataFolder = KitFiles.getPluginDataFolder();
		
		
		/*
		 * Check Directories.
		 */
		if (!pluginDataFolder.exists()){
			pluginDataFolder.mkdir();
		}
		
		if (!kitDataFolder.exists()){
			kitDataFolder.mkdir();
		}
		
		/*
		 * Check Files.
		 */
		File configFile = KitFiles.getConfigFile();
		
		if(!configFile.exists()){
			KitFiles.exportResource(KitUpgrades.class.getResource("/config.yml"), configFile);
		}
		if(!ppd.exists()){
			try {
				ppd.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if(!pdd.exists()){
			try {
				pdd.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if(!pld.exists()){
			try {
				pld.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}

}
