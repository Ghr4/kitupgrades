package kit.upgrades.plug.events;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import kit.upgrades.plug.enums.GeneralConfigType;
import kit.upgrades.plug.enums.Relic;
import kit.upgrades.plug.enums.UpgradeLevel;
import kit.upgrades.plug.enums.UpgradeType;
import kit.upgrades.plug.files.KitDataManager;
import kit.upgrades.plug.main.KitUpgrades;
import kit.upgrades.plug.upgrade.UpgradeManager;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.ItemSpawnEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitScheduler;

public class CombatEvents implements Listener{
	Plugin plugin;
	
	public CombatEvents(KitUpgrades plugin){
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
		this.plugin = plugin;
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerDeath(PlayerDeathEvent e){
		if(e.getEntity() instanceof Player){
			Player player = (Player)e.getEntity();
			awardKillPoint(KitDataManager.playerDamageMap.get(player.getUniqueId()), player.getUniqueId());
			JoinEvents.resetPlayerLevels(player);
			
			/*
			 * Handle all the inventory needs.
			 */
			List<ItemStack> defaultItems = new ArrayList<ItemStack>();
			List<ItemStack> keptItems = new ArrayList<ItemStack>();
			String worldName = player.getWorld().getName();
			String cartelWorldName = KitDataManager.configGeneralMap.get(GeneralConfigType.CARTEL_WORLD);
			Integer maxSlot = !worldName.equalsIgnoreCase(cartelWorldName) ? 3 : 2;
			
			for(int i = 0;i < maxSlot;i++){
				if(player.getInventory().getItem(i) != null && player.getInventory().getItem(i).getType() != Material.AIR){
					defaultItems.add(player.getInventory().getItem(i));
				}
			}
			for(Relic relic : Relic.values()){
				ItemStack relItem = Relic.getRelicItem(relic);
				if (player.getInventory().contains(relItem)){
					keptItems.add(relItem);
					defaultItems.add(relItem);
				}
			}
			KitDataManager.playerDeathKeptItems.put(player.getUniqueId(), keptItems);
			KitDataManager.playerDeathItems.put(player.getLocation().getBlock().getLocation(), defaultItems);
			removeDefaultItemsKeyDelay(player.getLocation().getBlock().getLocation());
		}
		
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onDeathItemSpawn(ItemSpawnEvent e){
		
		Location spawnLoc = e.getLocation().getBlock().getLocation();
		if(e.getEntity() instanceof Item){
			Item drop = (Item)e.getEntity();
			Iterator<Entry<Location, List<ItemStack>>> it = KitDataManager.playerDeathItems.entrySet().iterator();
		    while (it.hasNext()) {
		        Map.Entry<Location, List<ItemStack>> pair = (Map.Entry<Location, List<ItemStack>>)it.next();
		        Location storedLoc = pair.getKey();
		        if(Math.abs(storedLoc.getBlockX() - spawnLoc.getBlockX()) <= 3 && Math.abs(storedLoc.getBlockY() - spawnLoc.getBlockY()) <= 3 && Math.abs(storedLoc.getBlockZ() - spawnLoc.getBlockZ()) <= 3){
		        	for(ItemStack storedItem : pair.getValue()){
		        		if(drop != null && storedItem != null && storedItem.equals(drop.getItemStack())){
		        			e.setCancelled(true);
		        		}
		        	}
				}
		    }
		}
		
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerRespawn(PlayerRespawnEvent e){
		Player player = e.getPlayer();
			WorldEvents.setDefaultSlotItem(player);
			if (KitDataManager.playerDeathKeptItems.get(player.getUniqueId()) != null){
				for(ItemStack item : KitDataManager.playerDeathKeptItems.get(player.getUniqueId())){
					player.getInventory().addItem(item);
				}
				KitDataManager.playerDeathKeptItems.put(player.getUniqueId(), new ArrayList<ItemStack>());
			}
			
	}
	
	
	
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerAttack(EntityDamageByEntityEvent e){
		if(e.getDamager() instanceof Player && StaminaManager.getStamina((Player)e.getDamager()) == 0){
			e.setCancelled(true);
			return;
			}
		/*
		 * Attack
		 */
		if(!e.isCancelled() && e.getEntity() instanceof Player && e.getDamager() instanceof Player){
			Player victim = (Player) e.getEntity(), attacker = (Player) e.getDamager();
			
			
			//This map is setup as follows( UUID=an attacker, Double=The total damage from that UUID )
			HashMap<UUID, Double> attackedMap = KitDataManager.playerDamageMap.get(victim.getUniqueId());
			
			
			/*
			 * Set the event damage
			 */
			if(attacker.getItemInHand() != null && getUpgradeType(attacker.getItemInHand().getType()) != null && 
					getUpgradeType(attacker.getItemInHand().getType()) == UpgradeType.SWORD){
				UpgradeType upType = UpgradeType.SWORD;
				UpgradeLevel upLevel = KitDataManager.playerLevelMap.get(attacker.getUniqueId()).get(upType);
				List<Relic> relics = RelicManager.getPlayerRelics(attacker);
				double finalDamage = relics.contains(Relic.STRENGTH) ? new Double(getDamage(upType, upLevel) + Relic.getStrengthDamageAmount()) : getDamage(upType, upLevel);
				e.setDamage(finalDamage);
				
				/*
				 * Add the player to the attacked map
				 */
				attackedMap.put(attacker.getUniqueId(), finalDamage);
				KitDataManager.playerDamageMap.put(victim.getUniqueId(), attackedMap);
				
				/*
				 * Unarmed event
				 */
			}else{
				attackedMap.put(attacker.getUniqueId(), e.getDamage());
				KitDataManager.playerDamageMap.put(victim.getUniqueId(), attackedMap);
			}
			StaminaManager.subtractStamina(attacker, 1);
			
			
			
			/*
			 * player attacked a mob
			 */
		}else if(!e.isCancelled() && e.getDamager() instanceof Player){
			Player attacker = (Player) e.getDamager();
			/*
			 * Set the event damage
			 */
			if(attacker.getItemInHand() != null && getUpgradeType(attacker.getItemInHand().getType()) != null && 
					getUpgradeType(attacker.getItemInHand().getType()) == UpgradeType.SWORD){
				UpgradeType upType = UpgradeType.SWORD;
				UpgradeLevel upLevel = KitDataManager.playerLevelMap.get(attacker.getUniqueId()).get(upType);
				List<Relic> relics = RelicManager.getPlayerRelics(attacker);
				double finalDamage = relics.contains(Relic.STRENGTH) ? new Double(getDamage(upType, upLevel) + Relic.getStrengthDamageAmount()) : getDamage(upType, upLevel);
				e.setDamage(finalDamage);
			}
			StaminaManager.subtractStamina(attacker, 1);
			
			
			
			
			/*
			 * Archery
			 */
		}else if(e.getDamager() instanceof Arrow && e.getEntity() instanceof Player){
			Arrow arrow = (Arrow) e.getDamager();
			
			if(arrow.getShooter() instanceof Player){
				Player shooter = (Player) arrow.getShooter(), victim = (Player) e.getEntity();
				
				//This map is setup as follows( UUID=an attacker, Double=The total damage from that UUID )
				HashMap<UUID, Double> attackedMap = KitDataManager.playerDamageMap.get(victim.getUniqueId());
				
				
				/*
				 * Set the event damage
				 */
				UpgradeType upType = UpgradeType.BOW;
				UpgradeLevel upLevel = KitDataManager.playerLevelMap.get(shooter.getUniqueId()).get(upType);
				List<Relic> relics = RelicManager.getPlayerRelics(shooter);
				double finalDamage = relics.contains(Relic.STRENGTH) ? new Double(getDamage(upType, upLevel) + Relic.getStrengthDamageAmount()) : getDamage(upType, upLevel);
				e.setDamage(finalDamage);
				
				/*
				 * Add the player to the attacked map
				 */
				attackedMap.put(shooter.getUniqueId(), finalDamage);
				KitDataManager.playerDamageMap.put(victim.getUniqueId(), attackedMap);

			}
		}
	}
	
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerShootBow(EntityShootBowEvent e){
		if (e.getProjectile() instanceof Arrow && e.getEntity() instanceof Player && !e.isCancelled()){
			Player player = (Player)e.getEntity();
			if(StaminaManager.getStamina(player) == 0){
				e.setCancelled(true);
				}else{
					StaminaManager.subtractStamina(player, 2);
				}
			
		}
	}
	
	private void awardKillPoint(HashMap<UUID, Double> map, UUID deadPlayer) {
		if(map != null && !map.isEmpty()){
		   double max = Double.NEGATIVE_INFINITY;
		   double d;
		   UUID playerUUID = null;
		   
			    Iterator<Entry<UUID, Double>> it = map.entrySet().iterator();
			    while (it.hasNext()) {
			        Map.Entry<UUID, Double> pair = (Map.Entry<UUID, Double>)it.next();
			        d = pair.getValue();
			        
			        if (d > max){ 
			        	max = d;
			        	playerUUID = pair.getKey();
			        }
			    }
			    
			    /*
			     * Award points and upgrade book
			     */
			    if (playerUUID != null){
			    	Player player = Bukkit.getServer().getPlayer(playerUUID);
			    	UpgradeManager.addUpgradePoints(playerUUID, 1);
			    	if(player != null && player.isOnline()){
			    		HashMap<Integer, ItemStack> overload = player.getInventory().addItem(UpgradeManager.getTriggerItem());
				    	if(overload != null && !overload.isEmpty() && player.isOnline()){
				    		player.getWorld().dropItemNaturally(player.getLocation(), overload.get(overload.size()));
				    	}
			    	}
			    	
			    }
			}
		KitDataManager.playerDamageMap.put(deadPlayer, new HashMap<UUID, Double>());
		}
	
	private double getDamage(UpgradeType upType, UpgradeLevel upLevel){
			HashMap<UpgradeLevel, String> configDamageMap = KitDataManager.configLevelsMap.get(upType);
			return Double.valueOf(configDamageMap.get(upLevel));
		
	}
	private UpgradeType getUpgradeType(Material m){
		switch(m){
		case WOOD_SWORD:
		case STONE_SWORD:
		case IRON_SWORD:
		case GOLD_SWORD:
		case DIAMOND_SWORD:
		return UpgradeType.SWORD;
		
		case BOW:
			return UpgradeType.BOW;
		default:
			break;
		}
		return null;
	}
	private void removeDefaultItemsKeyDelay(final Location key){
        BukkitScheduler scheduler = Bukkit.getServer().getScheduler();
        scheduler.scheduleSyncDelayedTask(plugin, new Runnable() {
            @Override
            public void run() {
                KitDataManager.playerDeathItems.remove(key);
            }
        }, 200L);
	}
}
