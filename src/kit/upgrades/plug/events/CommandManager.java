package kit.upgrades.plug.events;

import kit.upgrades.plug.enums.Relic;
import kit.upgrades.plug.main.KitUpgrades;
import kit.upgrades.plug.upgrade.UpgradeGUI;
import kit.upgrades.plug.upgrade.UpgradeManager;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class CommandManager implements CommandExecutor{
	private Plugin plugin;
 public CommandManager(KitUpgrades plugin) {
		this.plugin = plugin;
	}
	
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if ((label.equalsIgnoreCase("ku") || label.equalsIgnoreCase("kupgrades"))){
			
			if(args.length == 1 && args[0].equalsIgnoreCase("reload")){
			if (sender instanceof Player && sender.hasPermission("kitupgrades.admin")){
				KitUpgrades.reload();
				sender.sendMessage(ChatColor.RED + "[KitUpgrades] Reloaded.");
				return true;
			}else if (!(sender instanceof Player)){
				KitUpgrades.reload();
				return true;
			}
		  }
			if (sender instanceof Player && sender.hasPermission("kitupgrades.admin")){
				Player player = (Player)sender;
				if((args.length == 2 || args.length == 3) && args[0].equalsIgnoreCase("give")){
					if(args.length == 2 && args[1].equalsIgnoreCase("trigger")){
						player.getInventory().addItem(UpgradeManager.getTriggerItem());
						return true;
						
					}else if(args.length == 3 && args[1].equalsIgnoreCase("relic")){
						
						if(args[2].equalsIgnoreCase("health")){
							player.getInventory().addItem(Relic.getRelicItem(Relic.HEALTH));
							return true;
						}else if(args[2].equalsIgnoreCase("mana") || args.length == 3 && args[2].equalsIgnoreCase("stamina")){
							player.getInventory().addItem(Relic.getRelicItem(Relic.STAMINA));
							return true;
						}else if(args[2].equalsIgnoreCase("strength")){
							player.getInventory().addItem(Relic.getRelicItem(Relic.STRENGTH));
							return true;
						}
						
					}
				}else if(args.length == 1 && args[0].equalsIgnoreCase("gui")){
					UpgradeGUI.displayGUI(player);
					return true;
				}
				
			}
			
		}
		
		if(sender instanceof Player){
			Player player = (Player) sender;
			player.sendMessage(ChatColor.DARK_AQUA+"_________________[KitUpgrades]_________________");
			player.sendMessage(ChatColor.AQUA + "Version: " + plugin.getDescription().getVersion());
			player.sendMessage(ChatColor.AQUA + "Created by: deadlyscone.");
			player.sendMessage(ChatColor.AQUA + "______________________________________________");
			if (sender.hasPermission("kitupgrades.admin")){
				player.sendMessage(ChatColor.GOLD + "---------------[Commands]---------------");
				player.sendMessage(ChatColor.RED + "/ku reload  -  " + ChatColor.GREEN + "Reloads the plugin data and config.");
				player.sendMessage(ChatColor.RED + "/ku give trigger  -  " + ChatColor.GREEN + "Gives the kit GUI upgrade item.");
				player.sendMessage(ChatColor.RED + "/ku give relic { health, mana, strength }  -  " + ChatColor.GREEN + "Gives the specified relic item.");
				player.sendMessage(ChatColor.RED + "/ku gui  -  " + ChatColor.GREEN + "opens the upgrade gui.");
			}
		}else{
			System.out.println("[KitUpgrades] Commands: /ku reload");
		}
		
		return true;
	}

}
