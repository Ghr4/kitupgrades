package kit.upgrades.plug.events;

import java.util.List;
import java.util.UUID;

import kit.upgrades.plug.enums.Relic;
import kit.upgrades.plug.enums.UpgradeLevel;
import kit.upgrades.plug.enums.UpgradeType;
import kit.upgrades.plug.files.KitDataManager;
import kit.upgrades.plug.main.KitUpgrades;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityRegainHealthEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitScheduler;

public class HealthManager implements Listener{
	
	private static Plugin plugin;
	public HealthManager(KitUpgrades plugin){
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
		HealthManager.plugin = plugin;
	}

	@EventHandler
	public void onPlayerHealthRegen(EntityRegainHealthEvent e){
		if (e.getEntity() instanceof Player){
			e.setCancelled(true);
		}
	}
	public static Double getPlayerHealth(Player player){
		return (player.getHealth() / player.getMaxHealth() * player.getHealthScale());
	}
	
	public static void setHealthScale(Player player){
		UpgradeLevel upLevel = KitDataManager.playerLevelMap.get(player.getUniqueId()).get(UpgradeType.HEALTH);
		Double amt = Double.valueOf(KitDataManager.configLevelsMap.get(UpgradeType.HEALTH).get(upLevel).split(",")[0]);
		player.setHealthScale(amt);
	}
	
	public static void setHealth(Player player, Double amt){
		double f = new Double(player.getMaxHealth() / player.getHealthScale());
		double g = new Double(f * amt);
		player.setHealth(g);
	}
	
	public static void addHealth(Player player, Double amt){
		double f = new Double(player.getMaxHealth() / player.getHealthScale());
		double g = new Double(f * amt);
		
		
		if((getPlayerHealth(player) + g) >= player.getHealthScale()){
			player.setHealth(player.getHealthScale() * f);
		}else{
			player.setHealth(player.getHealth() + g);
		}
		
	}
	
	public static void runHealthRegeneration(){
	    BukkitScheduler scheduler = Bukkit.getServer().getScheduler();

		scheduler.scheduleSyncRepeatingTask(plugin, new Runnable() {
	        @Override
	        public void run() {
	        	for (Player p : Bukkit.getServer().getOnlinePlayers()){
	        		if (!p.isDead()){
	        			UUID uid = p.getUniqueId();
	        			UpgradeLevel upLevel = KitDataManager.playerLevelMap.get(uid).get(UpgradeType.HEALTH);
		        		Double amt = Double.valueOf(KitDataManager.configLevelsMap.get(UpgradeType.HEALTH).get(upLevel).split(",")[1]);
	        			List<Relic> relics = RelicManager.getPlayerRelics(p);
	        			if(relics.contains(Relic.HEALTH)){
	        				Double a = amt;
	        				amt = new Double(a + Relic.getHealthRegenAmount());
	        			}
		        		
		        		addHealth(p, amt);
		        		RelicManager.getPlayerRelics(p);
	        		}
	        	}
	        }
	    }, 0L, (long)20);
	}
}
